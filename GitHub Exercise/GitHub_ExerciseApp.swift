//
//  GitHub_ExerciseApp.swift
//  GitHub Exercise
//
//  Created by Norm on 1/21/23.
//

import SwiftUI

@main
struct GitHub_ExerciseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
